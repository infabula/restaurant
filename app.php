<?php
require_once('autoloader.php');

use \Models\Kitchen;
//use \Repositories\ChickenEggRepository;
//use \Repositories\NeighbourEggRepository;
use \Models\Table;
use \Models\Container;

// what providers do we momentarily need

$config = [
    'providers' => array(
        'Providers\IngredientServiceProvider'
        )
    ];

// TODO: translation from App::bind() to $app->bind() needed

try
{
    $app = new Container();
    $app->bebob = 'Blabla';
    echo 'Bebob: ' . $app->bebob . PHP_EOL;

    $app->registerProviders($config['providers']);

    $eggRepository = $app->make('EggRepositoryInterface');

    $kitchen = new Kitchen($eggRepository);

    $kitchen->makeFriedEggs();

    $table = new Table();
    $app['table'] = $table;
    $t = $app['table'];
    #var_dump($t);
}
catch (Exception $e){
    echo $e->getMessage();
}

?>