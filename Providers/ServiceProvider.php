<?php

namespace Providers;

use \Models\Container;

abstract class ServiceProvider {

    protected $app;

    function __construct(Container $app){
        $this->app = $app;
    }

    abstract public function register();
}