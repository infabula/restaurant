<?php

namespace Providers;

use \Providers\ServiceProvider;
use \Repositories\ChickenEggRepository;
use \Repositories\NeighbourEggRepository;


class IngredientServiceProvider extends ServiceProvider {

    public function register(){
        //$this->app->bind('EggRepositoryInterface', '\Repositories\NeighbourEggRepository');
        $this->app->bind('EggRepositoryInterface', '\Repositories\ChickenEggRepository');
    }

}