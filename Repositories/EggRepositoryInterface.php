<?php

namespace Repositories;

/** Contract: 
    The next (only public!) methods MUST be implemented.
*/

interface EggRepositoryInterface {

    public function getEggs($count);

}