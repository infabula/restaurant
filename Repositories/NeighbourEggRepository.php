<?php

namespace Repositories;

use \Repositories\EggRepositoryInterface;

class NeighbourEggRepository implements EggRepositoryInterface {

    protected $neighbourEggCount = 12;
    
    function __construct(){
    }
    
    public function getEggs($count){
        echo 'Checking with the neighbours for ' . $count . ' eggs' . PHP_EOL;
        if($count > $this->neighbourEggCount){
            throw Exception('The neighbours only have ' . $this->neighbourEggCount . 'of eggs left.');
        }
        $this->neighbourEggCount -= $count;
        return $count;
    }

}