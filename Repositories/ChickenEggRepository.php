<?php

namespace Repositories;

use \Repositories\EggRepositoryInterface;
use Exception;

class ChickenEggRepository implements EggRepositoryInterface {

    protected $totalNumberOfEggs = 6;
    
    function __construct(){
    }

    public function setTotalNumberOfEggs($count){
        if ($count >= 0){
            $this->totalNumberOfEggs = $count;
        }
    }
    
    public function getEggs($count){
        echo 'Checking the chickens for ' . $count . ' eggs' . PHP_EOL;
        if($count > $this->totalNumberOfEggs){
            throw new Exception('There are only ' . $this->totalNumberOfEggs . 'of eggs left');
        }
        $this->totalNumberOfEggs -= $count;
        return $count;
    }

}