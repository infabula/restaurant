<?php

namespace Models;

class Table {

    private static $tableCounter = 1; 

    private $number = 0;
    private $reserved = false;
    private $nbOfSeats = 2;
    private $payed = false;
    private $clients = [];

    function __construct($nbOfSeats=2){
        $this->number = self::$tableCounter++;
        $this->nbOfSeats = $nbOfSeats;

        echo 'Creating Table with number ' . $this->number
             . ' and ' . $this->nbOfSeats . ' seats' . PHP_EOL; 
    }

    public function isEmpty() {
        if (count($this->clients) == 0){
            return true;
        }
        return false;
    }
}

/*
$table_1 = new Table(6); 
$table_2 = new Table(4); 
*/