<?php

namespace Models;

use ArrayObject;

class Container extends ArrayObject
{
    function __construct(){
        echo 'Constructing a Container' . PHP_EOL;
    }
    
    public function make($alias){
        // find alias
        $abstract = $this->$alias;
        if(isset($abstract))
        {
            // TODO: find all dependency injected instances
            // by using inspection
            $instance = new $abstract();
            return $instance;
        }
        throw new Exception('No class found for alias ' . $alias);
    }

    public function bind($alias, $abstract){
        echo "Binding " . $alias . ' to ' . $abstract . PHP_EOL;
        $this->$alias = $abstract;
        
    }

    public function registerProviders($providers){
        foreach($providers as $provider){
            echo "Registering ServiceProvider '" . $provider . "'" . PHP_EOL;
            // TODO: store instances
            $instance = new $provider($this);
            $instance->register();
        }
    }
}