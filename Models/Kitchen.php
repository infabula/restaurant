<?php
namespace Models;

use Repositories\EggRepositoryInterface;
    
class Kitchen {

    protected $eggRepository;

    // use type hinting to force only Fridge types
    // problem: only Fridge (inherited) classes allowed
    function __construct(EggRepositoryInterface $eggRepo){
        $this->eggRepository = $eggRepo;
    }

    public function makeFriedEggs(){
        echo 'Making fried eggs' . PHP_EOL;
        $nbOfEggs = 3;
        
        $eggs = $this->eggRepository->getEggs($nbOfEggs);
        echo 'Heating the pan' . PHP_EOL;
        echo 'Adding the ' . $eggs . ' eggs' . PHP_EOL;
        echo 'Wait...' . PHP_EOL;
        echo 'Done!' . PHP_EOL;
    }
}