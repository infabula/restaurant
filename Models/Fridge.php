<?php
namespace Models;

class Fridge {

    function __construct(){
        echo 'Creating a Fridge' . PHP_EOL;
    }

    public function getEggs($count){
        echo 'Getting ' . $count . ' eggs from the Fridge' . PHP_EOL;
    }

    public function getFish($count){
        echo 'Getting ' . $count . ' fish from the Fridge' . PHP_EOL;
    }
}